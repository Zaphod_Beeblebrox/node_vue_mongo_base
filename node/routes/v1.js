var express = require('express');
var router = express.Router();

const passport = require('passport');
const path = require('path');

// user routes
router.post('/users/login', UserController.login);

module.exports = router;
