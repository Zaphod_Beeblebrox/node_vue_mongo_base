'use strict';
var express = require('express');
var favicon = require('serve-favicon');
var cors = require('cors')
var bodyParser = require('body-parser');
var passport = require('passport');
var mongodb = require('mongodb');
var client = mongodb.MongoClient;
const {to} = require('await-to-js');
var db;
var v1 = require('./routes/v1');

// Constants
const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({'extended': 'false'}));

// setup passport
app.use(passport.initialize());

// setup cors
app.use(cors());

// for insert/update look at findAndModify in MongoDB that looks promising
client.connect('mongodb://mongodb', (err, client) => {
    if (err) return console.log(err);
    db = client.db('test');

    app.use('/v1', v1);

    app.use('/', function(req, res) {
        res.statusCode = 200;
        res.json({status: "Success", message: "API"});
    });

    // catch 404 and forward to error handler
    app.use(function (req, res, next) {
        var err = new Error('Not Found');
        err.status = 404;
        next(err);
    });

// restful api error handler
    app.use(function (err, req, res, next) {
        console.log(err);

        if (req.app.get('env') !== 'development') {
            delete err.stack;
        }

        res.status(err.statusCode || 500).json(err);
    });


    /*
     app.listen(port, () => {
     console.log('listening on port ' + PORT);
     app.get("/", cors(), function(req, res) {
     res.status(200).json(['hi']);
     });
     
     app.get('/')
     });
     */
});

// Generic error handler used by all endpoints.
function handleError(res, reason, message, code) {
    console.log("ERROR: " + reason);
    res.status(code || 500).json({"error": message});
}

module.exports = app;
