import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        test: 0
    },
    actions: {
    },
    mutations: {
    },
    getters: {
    },
    modules: {
    }
});

export default store;