import Vue from 'vue';
import Router from 'vue-router';

// route components
import About from './views/about.vue';
import Sprints from './views/sprints.vue';
import Reports from './views/reports.vue';

export const routes = [
    {
        path: '/',
        name: 'Home'
    }, {
        path: '/about',
        name: 'About',
        component: About
    }, {
        path: '/sprints',
        name: 'Sprints',
        component: Sprints
    }, {
        path: '/reports',
        name: 'Reports',
        component: Reports
    }
];