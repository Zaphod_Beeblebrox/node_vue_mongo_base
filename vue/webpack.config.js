var path = require("path");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const UglifyJs = require('uglifyjs-webpack-plugin');
var webpack = require('webpack')

const PATHS = {
    app: path.join(__dirname, './'),
    css: path.join(__dirname, './assets/css'),
    sass: path.join(__dirname, './assets/sass'),
    img: path.join(__dirname, './assets/img'),
    build: path.join(__dirname, 'dist')
}

// Main Settings config

module.exports = {
    entry: './app.js',
    output: {
        filename: 'build.js',
        path: PATHS.build
    },
    resolve: {
        alias: {
            vue: 'vue/dist/vue.js',
            node: 'node_modules',
            '@': path.resolve(__dirname, 'src'),
        }
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            }, {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        'css-loader'
                    ]
                })
            }, {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        'css-loader',
                        'sass-loader'
                    ]
                })
            }, {
                test: /\.js$/,
                exclude: /(node_modules)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            '@babel/preset-env'
                        ],
                        plugins: [
                            require('@babel/plugin-proposal-object-rest-spread')
                        ]
                    }
                }
            }, {
                test: /\.(png|svg|jpg|gif)$/,
                use: {
                    loader: 'file-loader',
                    options: {
                        outputPath: 'images/'
                    }
                },
            }
        ]
    },
    plugins: [
        new ExtractTextPlugin('build.css'),
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: '"production"'
            }
        }),
        new CleanWebpackPlugin(['dist']),
        new HtmlWebpackPlugin({
            title: 'nAgile',
            template: './src/index.html'
        })
            //new webpack.optimize.UglifyJsPlugin()
    ]
};
