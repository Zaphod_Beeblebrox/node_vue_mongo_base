// instantiate vue
window.Vue = require('vue');
import VueRouter from 'vue-router';
import {routes} from '@/router.js';
import store from '@/store.js';
import lodash from 'lodash';
import App from './app.vue';

// instantiate Vue
import moment from 'moment';
window.moment = moment;

// router
Vue.use(VueRouter);
let router = new VueRouter({hashbang: false, mode: 'history', routes});

// define components
/*
Vue.component('projectManage', require('./components/projects/manage.vue'));
Vue.component('projectAbout', require('./components/projects/about.vue'));
Vue.component('stories', require('./components/story/workcolumn.vue'));
Vue.component('storycard', require('./components/story/storycard.vue'));
Vue.component('storyview', require('./components/story/view.vue'));
Vue.component('taskview', require('./components/tasks/list.vue'));
Vue.component('comments', require('./components/story/comments.vue'));
*/

// css/sass imports
import './assets/sass/app.scss';
import './assets/css/app.css';
    
// main
new Vue({
    el: '#app',
    template: '<App/>',
    //components: { App },
    router,
    store,
    render: h => h(App)
});
